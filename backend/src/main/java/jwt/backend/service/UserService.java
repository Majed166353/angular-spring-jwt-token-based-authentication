package jwt.backend.service;

import jwt.backend.entity.User;
import jwt.backend.entity.dto.UserDto;
import jwt.backend.security.exception.*;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;

public interface UserService {

    User register(UserDto userDto) throws EmailExistException, UsernameExistsException, UserNotFoundException, MessagingException;

    List<User> getUsers(String searchValueForFirstName,
                        String searchValueForLastName,
                        String searchValueForUsername,
                        String searchValueForEmail,
                        Integer page, Integer size);

    User findUserByUsername(String username);

    User findUserByEmail(String email);

    User addNewUser(String firstName,
                    String lastName,
                    String username,
                    String email,
                    String role,
                    boolean isNonLocked,
                    boolean isActive,
                    MultipartFile profileImage
    ) throws UserNotFoundException, EmailExistException, UsernameExistsException, IOException, NotAnImageFileException, MessagingException;

    void deleteUser(String username);

    void restPassword(String email) throws MessagingException, EmailNotFoundException;

    User updateProfileImage(String username, MultipartFile profileImage) throws UserNotFoundException, EmailExistException, UsernameExistsException, IOException, NotAnImageFileException;

    void updateUser(String currentUsername,
                    String newFirstName,
                    String newLastName,
                    String newUsername,
                    String newEmail,
                    String role,
                    boolean isNonLocked,
                    boolean isActive,
                    MultipartFile profileImage
    ) throws UserNotFoundException, EmailExistException, UsernameExistsException, IOException, NotAnImageFileException;

    Long count();
}
