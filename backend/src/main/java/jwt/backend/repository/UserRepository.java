package jwt.backend.repository;

import jwt.backend.entity.User;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {
    @Query("SELECT U FROM User U " +
            "WHERE LOWER(U.firstName) LIKE %:searchValueForFirstName% AND LOWER(U.lastName) LIKE %:searchValueForLastName% AND LOWER(U.username) LIKE %:searchValueForUsername% AND LOWER(U.email)  LIKE %:searchValueForEmail% AND U.deletedAt is null order by U.id DESC")
    List<User> findByDeletedAtNull(
            @Param("searchValueForFirstName") String searchValueForFirstName,
            @Param("searchValueForLastName") String searchValueForLastName,
            @Param("searchValueForUsername") String searchValueForUsername,
            @Param("searchValueForEmail") String searchValueForEmail,
            PageRequest pageable);

    User findUserByUsername(String name);
    User findUserByEmail(String email);
    Long countAllByDeletedAtNull();
    void deleteUserByUsername(String username);
}
