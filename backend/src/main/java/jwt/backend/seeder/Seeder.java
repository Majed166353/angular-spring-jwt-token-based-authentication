package jwt.backend.seeder;

import jwt.backend.entity.User;
import jwt.backend.repository.UserRepository;
import jwt.backend.security.enumeration.Role;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Date;

/**
 * @author Jaber
 * @date 7/5/2022
 * @time 4:53 PM
 */
@Slf4j
@Configuration
@AllArgsConstructor
public class Seeder {
    private final UserRepository userRepository;

    @Bean
    public void seed() {
        seedUsersTable();
    }

    void seedUsersTable() {
//       Pass: Test12345
//       Hash: $2a$12$A3ym9LJIEmJTKDdGhLyKUO0x3SC0jdaUr61qKtyILjLK8kQJLCeh.
        if (userRepository.count() < 5) {
            User superAdmin = new User();
            superAdmin.setFirstName("SuperAdmin");
            superAdmin.setLastName("SuperAdmin");
            superAdmin.setUsername("superadmin");
            superAdmin.setEmail("SuperAdmin@gmail.com");
            superAdmin.setRole(getRoleEnumName("ROLE_SUPER_ADMIN").name());
            superAdmin.setAuthorities(getRoleEnumName("ROLE_SUPER_ADMIN").getAuthorities());
            superAdmin.setActive(true);
            superAdmin.setNotLocked(true);
            superAdmin.setCreatedAt(new Date());
            userRepository.save(superAdmin);

            User admin = new User();
            admin.setFirstName("Admin");
            admin.setLastName("Admin");
            admin.setUsername("admin");
            admin.setEmail("Admin@gmail.com");
            admin.setRole(getRoleEnumName("ROLE_ADMIN").name());
            admin.setAuthorities(getRoleEnumName("ROLE_ADMIN").getAuthorities());
            admin.setActive(true);
            admin.setNotLocked(true);
            admin.setCreatedAt(new Date());
            userRepository.save(admin);

            User manager = new User();
            manager.setFirstName("Manager");
            manager.setLastName("Manager");
            manager.setUsername("manager");
            manager.setEmail("Manager@gmail.com");
            manager.setRole(getRoleEnumName("ROLE_MANAGER").name());
            manager.setAuthorities(getRoleEnumName("ROLE_MANAGER").getAuthorities());
            manager.setActive(true);
            manager.setNotLocked(true);
            manager.setCreatedAt(new Date());
            userRepository.save(manager);

            User hr = new User();
            hr.setFirstName("HR");
            hr.setLastName("HR");
            hr.setUsername("hr");
            hr.setEmail("HR@gmail.com");
            hr.setRole(getRoleEnumName("ROLE_HR").name());
            hr.setAuthorities(getRoleEnumName("ROLE_HR").getAuthorities());
            hr.setActive(true);
            hr.setNotLocked(true);
            hr.setCreatedAt(new Date());
            userRepository.save(hr);

            User user = new User();
            user.setFirstName("User");
            user.setLastName("User");
            user.setUsername("user");
            user.setEmail("User@gmail.com");
            user.setRole(getRoleEnumName("ROLE_USER").name());
            user.setAuthorities(getRoleEnumName("ROLE_USER").getAuthorities());
            user.setActive(true);
            user.setNotLocked(true);
            user.setCreatedAt(new Date());
            userRepository.save(user);
            log.info("Users Seeded");
        }
    }

    private Role getRoleEnumName(String role) {
        return Role.valueOf(role.toUpperCase());
    }
}
