package jwt.backend.security.exception;

/**
 * @author Jaber
 * @Date 8/31/2022
 * @Time 8:55 PM
 * @Project backend
 */
public class NotAnImageFileException extends Exception {
    public NotAnImageFileException(String message) {
        super(message);
    }
}
