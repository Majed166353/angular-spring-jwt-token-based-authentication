package jwt.backend.security.constant;

/**
 * @author Jaber
 * @Date 8/24/2022
 * @Project backend
 */
public class AppConstant {
    /* ===============================================================File Constant=========================================================== */
    public static final String USER_IMAGE_PATH = "/user/image/";
    public static final String JPG_EXTENSION = "jpg";
    public static final String USER_FOLDER = System.getProperty("user.home") + "/supportportal/user/";
    public static final String DEFAULT_USER_IMAGE_PATH = "/user/image/profile/";
    public static final String DOT = ".";
    public static final String FORWARD_SLASH = "/";
    public static final String TEMP_PROFILE_IMAGE_BASE_URL = "https://robohash.org/";
    /* ===============================================================Security Constant=========================================================== */
    public static final long EXPIRATION_TIME = 432_000_000; //5 DAYS EXPRESSED IN MILLISECOND;
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String JWT_TOKEN_HEADER = "Jwt-Token";
    public static final String TOKEN_CANNOT_BE_VERIFIED = "Token can't be verified!";
    public static final String GET_ARRAYS_LLC = "Get Arrays, LLC";
    public static final String GET_ARRAYS_ADMINISTRATION = "User Management Portal";
    public static final String AUTHORITIES = "authorities";
    public static final String FORBIDDEN_MESSAGE = "You need to login to access this page";
    public static final String ACCESS_DENIED_MESSAGE = "You do not have enough permission!";
    public static final String OPTION_HTTP_METHOD = "Options";
    public static final String[] PUBLIC_URLS = {
            "/user/login",
            "/user/register",
            "/user/image/**"
    };
    /* ===============================================================User Implementation Constant=========================================================== */
    public static final String USERNAME_ALREADY_EXISTS = "Username already exists";
    public static final String EMAIL_ALREADY_EXISTS = "Email already exists";
    public static final String NO_USER_FOUND_BY_USERNAME = "No user found by username";
    public static final String FOUND_USER_BY_USERNAME = "Returning found user by username";
    public static final String EMAIL_SENT = "An email with new password with sent to: ";
    public static final String USER_DELETED_SUCCESSFULLY = "User deleted successfully!";
    public static final String NO_USER_FOUND_BY_EMAIL = "No user found for email: ";
    /* ===============================================================Email Constant=========================================================== */
    public static final String SIMPLE_MAIL_TRANSFER_PROTOCOL = "smtps";
    public static final String USERNAME = "Majedabdullah635@gmail.com";
    public static final String PASSWORD = "eezpphlcbiobovup";
    public static final String FROM_EMAIL = "Majedabdullah635@gmail.com";
    public static final String CC_EMAIL = "Helloworld1663@gmail.com";
    public static final String EMAIL_SUBJECT = "User management system";
    public static final String GMAIL_SMTP_SERVER = "smtp.gmail.com";
    public static final String SMTP_HOST = "mail.smtp.host";
    public static final String SMTP_AUTH = "mail.smtp.auth";
    public static final String SMTP_PORT = "mail.smtp.port";
    public static final int DEFAULT_PORT = 465;
    public static final String SMTP_STARTTLS_ENABLE = "mail.smtp.starttls.enable";
    public static final String SMTP_STARTTLS_REQUIRED = "mail.smtp.starttls.required";
}
