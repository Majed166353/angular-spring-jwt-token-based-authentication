import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthenticationService} from "./global-service/authentication.service";
import {NotificationService} from "./global-service/notification.service";

@Injectable({
  providedIn: 'root'
})
export class AppGuard implements CanActivate {
  constructor(private auth: AuthenticationService,
              private router: Router,
              private notificationService: NotificationService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.isUserLoggedIn();
  }

  isUserLoggedIn(): boolean {
    if (this.auth.isUserLoggedIn()) {
      return true;
    }
    this.router.navigate(['/login']);
    this.notificationService.error("You need to login to access this page!".toUpperCase());
    return false;
  }
}
