import {Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {Subscription} from "rxjs";
import {HttpErrorResponse, HttpResponse} from "@angular/common/http";
import {User} from "../../auth/component/user/user";
import {HeaderType} from "../../global-configuration/enum/header-type";
import {AuthenticationService} from "../../global-service/authentication.service";
import {NotificationService} from "../../global-service/notification.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  loginFormGroup: FormGroup;
  subscription: Subscription[] = [];

  constructor(private formBuilder: FormBuilder,
              private auth: AuthenticationService,
              private router: Router,
              private notificationService: NotificationService) {
    this.loginFormGroup = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(15)]],
      password: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(15)]]
    })
  }

  get formControls(): { [key: string]: AbstractControl } {
    return this.loginFormGroup.controls;
  }

  ngOnInit(): void {
    if (this.auth.isUserLoggedIn()) {
      this.router.navigateByUrl('/dashboard');
    } else {
      this.router.navigateByUrl('/login');
    }
  }

  login() {
    this.subscription.push(
      this.auth.login(this.loginFormGroup.value).subscribe({
        next: (response: HttpResponse<User>) => {
          this.auth.saveTokenToTheLocalStorage(response.headers.get(HeaderType.JWT_TOKEN));
          this.auth.saveUserToTheLocalStorage(response.body);
          this.router.navigateByUrl('/dashboard');
          this.notificationService.success("Successfully Logged In!");
        },
        error: (errorResponse: HttpErrorResponse) => {
          this.notificationService.error(errorResponse.error.message);
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.subscription.forEach(sub => sub.unsubscribe());
  }
}
