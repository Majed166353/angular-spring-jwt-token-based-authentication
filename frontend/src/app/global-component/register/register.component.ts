import {Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthenticationService} from "../../global-service/authentication.service";
import {Router} from "@angular/router";
import {NotificationService} from "../../global-service/notification.service";
import {HttpErrorResponse, HttpResponse} from "@angular/common/http";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {
  registerFormGroup: FormGroup;
  subscription: Subscription[] = [];

  constructor(private formBuilder: FormBuilder,
              private auth: AuthenticationService,
              private router: Router,
              private notificationService: NotificationService) {
    this.registerFormGroup = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(15)]],
      lastName: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(15)]],
      username: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(15)]],
      email: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(30)]],
    })
  }

  get formControls(): { [key: string]: AbstractControl } {
    return this.registerFormGroup.controls;
  }

  ngOnInit(): void {
  }

  register() {
    this.subscription.push(
      this.auth.register(this.registerFormGroup.value).subscribe({
        next: (response: HttpResponse<any>) => {
          this.router.navigateByUrl('/login');
          this.notificationService.success("Password sent to your email!");
        },
        error: (errorResponse: HttpErrorResponse) => {
          this.notificationService.error(errorResponse.error.message);
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.subscription.forEach(sub => sub.unsubscribe());
  }
}
