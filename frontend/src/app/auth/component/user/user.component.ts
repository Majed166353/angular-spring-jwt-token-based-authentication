import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {HttpErrorResponse, HttpResponse} from "@angular/common/http";
import {User} from "./user";
import {UserService} from "./user.service";
import {CommonService} from "../../../global-service/common.service";
import {NotificationService} from "../../../global-service/notification.service";
import {Subscription} from "rxjs";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CustomHttpResponse} from "../../../global-model/custom-http-response";
import {AuthenticationService} from "../../../global-service/authentication.service";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {

  userList: User[];
  size: number = 10;
  pageOffset = 1;
  itemsPerPage = 10;
  totalItems = 0;
  searchValueForFirstName: string = "";
  searchValueForLastName: string = "";
  searchValueForUsername: string = "";
  searchValueForEmail: string = "";
  subscription: Subscription[] = [];

  constructor(private userService: UserService,
              private commonService: CommonService,
              private notificationService: NotificationService,
              private dialog: MatDialog,
              public auth: AuthenticationService
  ) {
  }

  ngOnDestroy(): void {
    this.subscription.forEach(sub => sub.unsubscribe());
  }

  ngOnInit(): void {
    this.getUserList();
    this.commonService.aClickedEvent.subscribe((data: string) => {
      this.getUserList();
    });
  }

  create() {
    this.dialog.open(CreateEditNewUserDialog, {
      width: '400px', data: {
        isActive: true,
        isNonLocked: true,
        operation: 'Create',
      }
    });
  }

  edit(collection) {
    this.dialog.open(CreateEditNewUserDialog, {
      width: '400px', data: {
        id: collection.id,
        firstName: collection.firstName,
        lastName: collection.lastName,
        username: collection.username,
        email: collection.email,
        role: collection.role,
        profileImage: collection.profileImageUrl,
        isActive: collection.active,
        isNonLocked: collection.notLocked,
        operation: 'Update',
      }
    });
  }

  getUserList() {
    this.subscription.push(
      this.userService.getUsers(
        this.searchValueForFirstName,
        this.searchValueForLastName,
        this.searchValueForUsername,
        this.searchValueForEmail,
        this.pageOffset, this.itemsPerPage)
        .subscribe({
          next: (response: User[]) => {
            this.count();
            this.userList = response;
          },
          error: (errorResponse: HttpErrorResponse) => {
            this.notificationService.error(errorResponse.error.message);
          }
        })
    );
  }

  delete(username) {
    this.subscription.push(
      this.userService.deleteUser(username)
        .subscribe({
          next: (response: CustomHttpResponse) => {
            this.commonService.AClicked();
            this.notificationService.success("User Deleted Successfully");
            this.count();
          },
          error: (errorResponse: HttpErrorResponse) => {
            this.notificationService.error(errorResponse.error.message);
          }
        })
    );
  }

  pageChange(newPage: number) {
    this.pageOffset = newPage;
    this.getUserList();
  }

  searchByFirstname(event) {
    this.searchValueForFirstName = event;
    this.getUserList();
  }

  searchByLastName(event) {
    this.searchValueForLastName = event;
    this.getUserList();
  }

  searchByUsername(event) {
    this.searchValueForUsername = event;
    this.getUserList();
  }

  searchByEmail(event) {
    this.searchValueForEmail = event;
    this.getUserList();
  }

  itemsOnChange(event) {
    this.pageOffset = 1;
    this.itemsPerPage = event;
    this.getUserList();
  }

  count() {
    this.subscription.push(
      this.userService.count()
        .subscribe({
          next: (response: any) => {
            if (response) {
              this.totalItems = response;
            }
          },
        })
    );
  }

  resetPassword(email) {
    this.subscription.push(
      this.userService.resetPassword(email)
        .subscribe({
          next: (response: CustomHttpResponse) => {
            this.notificationService.success("An email sent to: " + email);
          }, error: (errorResponse: HttpErrorResponse) => {
            this.notificationService.error(errorResponse.error.message);
          }
        })
    );
  }
}

@Component({
  templateUrl: './create-edit-new-user.component.html',
  styleUrls: ['./user.component.css']

})
export class CreateEditNewUserDialog implements OnDestroy {
  userGroup: FormGroup;
  defaultImg = './assets/avatar/default.jpg';
  imageUrl = this.defaultImg;
  showRemoveButton = false;
  subscription: Subscription[] = [];
  private originalFile: File;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private refDialog: MatDialogRef<CreateEditNewUserDialog>,
              private commonService: CommonService,
              private formBuilder: FormBuilder,
              private notificationService: NotificationService,
              private userService: UserService,
              public auth: AuthenticationService) {
    this.userGroup = this.formBuilder.group({
      firstName: [this.data.firstName],
      lastName: [this.data.lastName],
      username: [this.data.username, [Validators.required, Validators.minLength(1)]],
      email: [this.data.email, [Validators.email, Validators.required, Validators.minLength(1)]],
      role: [this.data.role],
      isActive: [this.data.isActive, [Validators.required]],
      isNonLocked: [this.data.isNonLocked, [Validators.required]],
    });
    if (data.id) {
      this.imageUrl = data.profileImage;
    }
  }

  ngOnDestroy(): void {
    this.subscription.forEach(sub => sub.unsubscribe());
  }

  fileUploads(event: any) {
    if (event.target.files && event.target.files[0]) {
      this.originalFile = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (e: any) => {
        this.imageUrl = e.target.result;
        this.showRemoveButton = true;
      }
    }
  }

  basicRemoveImage() {
    this.imageUrl = null;
    this.showRemoveButton = false;
    this.imageUrl = this.defaultImg;
  }

  submit() {
    if (this.data.operation == 'Create') {
      const formData = this.userService.formData(null, this.userGroup, this.originalFile)
      this.subscription.push(
        this.userService.addUser(formData)
          .subscribe({
            next: (response: HttpResponse<any>) => {
              this.commonService.AClicked();
              this.notificationService.success("User added successfully!");
              this.refDialog.close();
            },
            error: (errorResponse: HttpErrorResponse) => {
              this.notificationService.error(errorResponse.error.message)
            }
          })
      );
    } else {
      const formData = this.userService.formData(this.userGroup.value.username, this.userGroup, this.originalFile)
      this.subscription.push(
        this.userService.updateUser(formData)
          .subscribe({
            next: (response: HttpResponse<any>) => {
              this.commonService.AClicked();
              this.notificationService.success("User updated successfully!");
              this.refDialog.close();
            },
            error: (errorResponse: HttpErrorResponse) => {
              this.notificationService.error(errorResponse.error.message)
            }
          })
      );
    }
  }
}
