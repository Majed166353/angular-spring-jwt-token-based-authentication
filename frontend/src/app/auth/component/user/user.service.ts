import {Injectable} from '@angular/core';
import {environment} from "../../../../environments/environment";
import {HttpClient, HttpErrorResponse, HttpEvent, HttpParams, HttpRequest, HttpResponse} from "@angular/common/http";
import {User} from "./user";
import {Observable} from "rxjs";
import {Form, FormGroup} from "@angular/forms";
import {CustomHttpResponse} from "../../../global-model/custom-http-response";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private host = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  getUsers(searchValueForFirstName: string,
           searchValueForLastName: string,
           searchValueForUsername: string,
           searchValueForEmail: string,
           page: number, size: number): Observable<User[]> {
    const reqParams = new HttpParams()
      .set('searchValueForFirstName', searchValueForFirstName)
      .set('searchValueForLastName', searchValueForLastName)
      .set('searchValueForUsername', searchValueForUsername)
      .set('searchValueForEmail', searchValueForEmail)
      .set('page', page)
      .set('size', size);
    return this.http.get<User[]>(`${this.host}/user/list`, {params: reqParams});
  }

  addUser(formData: FormData): Observable<HttpResponse<any> | HttpErrorResponse> {
    return this.http.post<HttpResponse<User>>(`${this.host}/user/add`, formData);
  }

  updateUser(formData: FormData): Observable<any> {
    return this.http.post<any>(`${this.host}/user/update`, formData);
  }

  resetPassword(email: string): Observable<CustomHttpResponse | HttpErrorResponse> {
    const reqParam = new HttpParams().set('email', email);
    return this.http.post<CustomHttpResponse>(`${this.host}/user/reset-password`, null, {params: reqParam});
  }

  updateProfileImage(formGroup: FormGroup): Observable<HttpEvent<User | HttpErrorResponse>> {
    return this.http.post<User | HttpErrorResponse>(`${this.host}/user/update-profile-image`, formGroup, {
      reportProgress: true,
      observe: 'events'
    });
  }

  deleteUser(userId: number): Observable<CustomHttpResponse | HttpErrorResponse> {
    return this.http.delete<CustomHttpResponse | HttpErrorResponse>(`${this.host}/user/delete/${userId}`);
  }

  addUsersToLocalCache(users: User[]): void {
    localStorage.setItem('users', JSON.stringify(users));
  }

  getUsersFromLocalCache(): User[] {
    if (localStorage.getItem('users')) {
      return JSON.parse(localStorage.getItem('users'));
    }
    return null;
  }

  count(): Observable<any | HttpErrorResponse> {
    return this.http.get<any>(this.host + '/user/count');
  }

  formData(loggedInUsername: string, formGroup: FormGroup, profileImage: File): FormData {
    const formData = new FormData();
    formData.append('currentUsername', loggedInUsername);
    formData.append('firstName', formGroup.value.firstName);
    formData.append('lastName', formGroup.value.lastName);
    formData.append('username', formGroup.value.username);
    formData.append('email', formGroup.value.email);
    formData.append('role', formGroup.value.role);
    formData.append('isActive', JSON.stringify(formGroup.value.isActive));
    formData.append('isNonLocked', JSON.stringify(formGroup.value.isNonLocked));
    formData.append('profileImage', profileImage);
    return formData;
  }
}
