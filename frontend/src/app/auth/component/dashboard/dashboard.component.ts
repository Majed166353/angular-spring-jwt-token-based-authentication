import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from "../../../global-service/authentication.service";
import {NotificationService} from "../../../global-service/notification.service";
import {Router} from "@angular/router";
import {LoaderService} from "../../../global-service/loader.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  isCollapsed: boolean = false;

  constructor(public auth: AuthenticationService,
              private router: Router,
              private notificationService: NotificationService,
              public loaderService: LoaderService) {
  }

  ngOnInit(): void {
  }

  logout() {
    this.auth.logOut();
    this.router.navigateByUrl('/login');
    this.notificationService.default("You've logged out to the system");
  }
}
