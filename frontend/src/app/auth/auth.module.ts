import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthRoutingModule} from './auth-routing.module';
import {CreateEditNewUserDialog, UserComponent} from './component/user/user.component';
import {NgxPaginationModule} from "ngx-pagination";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzCardModule} from "ng-zorro-antd/card";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzEmptyModule} from "ng-zorro-antd/empty";
import {NzImageModule} from "ng-zorro-antd/image";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzSelectModule} from "ng-zorro-antd/select";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {NzButtonModule} from "ng-zorro-antd/button";
import {MatDialogModule} from "@angular/material/dialog";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";


@NgModule({
  declarations: [
    UserComponent,
    CreateEditNewUserDialog,
  ],
    imports: [
        CommonModule,
        AuthRoutingModule,
        NgxPaginationModule,
        NzIconModule,
        NzInputModule,
        NzCardModule,
        FormsModule,
        NzEmptyModule,
        NzImageModule,
        NzFormModule,
        NzSelectModule,
        NzUploadModule,
        MatSlideToggleModule,
        ReactiveFormsModule,
        NzButtonModule,
        MatDialogModule,
        NzDividerModule,
        NzPopconfirmModule,
        NzToolTipModule
    ]
})
export class AuthModule {
}
