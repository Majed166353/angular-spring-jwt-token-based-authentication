import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {AuthenticationService} from "./global-service/authentication.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private auth: AuthenticationService,
              private router: Router) {
    this.checkIfUserIsLoggedIn();
  }

  checkIfUserIsLoggedIn() {
    if (this.auth.isUserLoggedIn()) {
      this.router.navigateByUrl('/dashboard');
    } else {
      this.router.navigateByUrl('/login');
    }
  }
}
