import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {AuthInterceptor} from "./global-configuration/interceptor/auth.interceptor";
import {NotificationModule} from "./global-configuration/module/notification.module";
import {RouterModule} from "@angular/router";
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {NzMenuModule} from "ng-zorro-antd/menu";
import {NzBreadCrumbModule} from "ng-zorro-antd/breadcrumb";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzIconModule} from "ng-zorro-antd/icon";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {DefaultRoutingModule} from "./default-routing.module";
import {DashboardComponent} from './auth/component/dashboard/dashboard.component';
import {AuthRoutingModule} from "./auth/auth-routing.module";
import {ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatCardModule} from "@angular/material/card";
import {MatButtonModule} from "@angular/material/button";
import {RegisterComponent} from "./global-component/register/register.component";
import {LoginComponent} from "./global-component/login/login.component";
import {AuthModule} from "./auth/auth.module";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {InterceptorService} from "./global-configuration/interceptor/interceptor.service";
import {NzImageModule} from "ng-zorro-antd/image";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    RegisterComponent,
  ],
    imports: [
        BrowserModule,
        HttpClientModule,
        NotificationModule,
        RouterModule,
        NzLayoutModule,
        NzMenuModule,
        NzBreadCrumbModule,
        NzDropDownModule,
        NzIconModule,
        BrowserAnimationsModule,
        DefaultRoutingModule,
        AuthRoutingModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatCardModule,
        MatButtonModule,
        AuthModule,
        MatProgressBarModule,
        NzImageModule
    ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
