import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import {JwtHelperService} from "@auth0/angular-jwt";
import {environment} from "../../environments/environment";
import {User} from "../auth/component/user/user";
import {FormGroup} from "@angular/forms";
import {Role} from "../global-configuration/enum/role";

@Injectable({
  providedIn: 'root'
})

/*--[Note]: {observe: 'response'} returns the whole response including   header. By default, http response returns the body only.--*/

export class AuthenticationService {
  public host = environment.apiUrl;
  private token: string;
  private loggedInUsername: string;
  private jwtHelper = new JwtHelperService();

  constructor(private http: HttpClient) {
  }


  login(user: FormGroup): Observable<HttpResponse<User>> {
    return this.http.post<User>(`${this.host}/user/login`, user, {observe: 'response'});
  }

  register(user: FormGroup): Observable<HttpResponse<any>> {
    return this.http.post<any>(`${this.host}/user/register`, user);
  }

  logOut(): void {
    this.token = null;
    this.loggedInUsername = null;
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    localStorage.removeItem('users');
  }

  saveTokenToTheLocalStorage(token: string): void {
    this.token = token;
    localStorage.setItem('token', token);
  }

  saveUserToTheLocalStorage(user: User): void {
    localStorage.setItem('user', JSON.stringify(user));
  }


  loadToken(): void {
    this.token = localStorage.getItem('token');
  }

  getToken(): string {
    return this.token;
  }

  isUserLoggedIn(): boolean {
    this.loadToken();
    if ((this.token != null && this.token !== '') && (this.jwtHelper.decodeToken(this.token).sub != null || '') && (!this.jwtHelper.isTokenExpired(this.token))) {
      this.loggedInUsername = this.jwtHelper.decodeToken(this.token).sub;
      return true;
    } else {
      this.logOut();
      return false;
    }
  }

  public get isManager(): boolean {
    return this.getUserRole() === Role.MANAGER || this.isSuperAdmin || this.isAdmin;
  }

  public get isHR(): boolean {
    return this.getUserRole() === Role.HR || this.isSuperAdmin || this.isAdmin;
  }

  public get isAdmin(): boolean {
    return this.getUserRole() === Role.ADMIN || this.isSuperAdmin;
  }

  public get isSuperAdmin(): boolean {
    return this.getUserRole() === Role.SUPER_ADMIN;
  }

  public get exceptUser(): boolean {
    return this.isManager || this.isHR || this.isAdmin || this.isSuperAdmin;
  }

  getUserFromLocalCache(): User {
    return JSON.parse(localStorage.getItem('user'));
  }

  getUserRole(): string {
    return this.getUserFromLocalCache().role;
  }
}
