import {EventEmitter, Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  host: string;
  aClickedEvent: EventEmitter<any> = new EventEmitter();

  constructor() {
    this.host = window.location.host;
  }

  AClicked(): void {
    this.aClickedEvent.emit();
  }
}
