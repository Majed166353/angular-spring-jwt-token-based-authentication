import {Injectable} from '@angular/core';
import {NotifierService} from "angular-notifier";
import {NotificationType} from "../global-configuration/enum/notification-type";

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private notifier: NotifierService) {
  }

  public default(message: string) {
    if (message) {
      this.notifier.notify(NotificationType.DEFAULT, this.capitalizeFirstLetter(message));
    } else {
      this.notifier.notify(NotificationType.DEFAULT, "Oops! Something went wrong.");
    }
  }

  public info(message: string) {
    if (message) {
      this.notifier.notify(NotificationType.INFO, this.capitalizeFirstLetter(message));
    } else {
      this.notifier.notify(NotificationType.INFO, "Oops! Something went wrong.");
    }
  }

  public success(message: string) {
    if (message) {
      this.notifier.notify(NotificationType.SUCCESS, this.capitalizeFirstLetter(message));
    } else {
      this.notifier.notify(NotificationType.SUCCESS, "Oops! Something went wrong.");
    }
  }

  public warning(message: string) {
    if (message) {
      this.notifier.notify(NotificationType.WARNING, this.capitalizeFirstLetter(message));
    } else {
      this.notifier.notify(NotificationType.WARNING, "Oops! Something went wrong.");
    }
  }

  public error(message: string) {
    if (message) {
      this.notifier.notify(NotificationType.ERROR, this.capitalizeFirstLetter(message));
    } else {
      this.notifier.notify(NotificationType.ERROR, "Oops! Something went wrong.");
    }
  }

  capitalizeFirstLetter(message): string {
    return message ? message.charAt(0).toUpperCase() + message.substring(1).toLowerCase() : '';
  }
}


